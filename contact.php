<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Octingen Solutions - Contact</title>
    <!-- style sheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
    <!-- gallery -->
    <link rel="stylesheert" href="css/grid-gallery.css">
    <link rel="stylesheet" href="css/baguetteBox.css">
    <link rel="stylesheet" href="css/animation.css">
</head>

<body>
    <!-- header -->
    <header class="fixed-top">
        <!-- top header -->
        <div class="top-header py-1">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 col-6 align-self-center">
                        <p class="headercontact">
                            <span>info@octingen.com </span>
                        </p>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6 col-6 align-self-center">
                        <div class="social-links float-right text-right">
                            <a href="https://www.facebook.com/Octingen-Pvt-Ltd-105527871073455" target="_blank"><span
                                    class="icon-facebook icomoon"></span></a>
                            <a href="https://twitter.com/octingen" target="_blank"><span
                                    class="icon-twitter icomoon"></span></a>
                            <a href="https://www.linkedin.com/in/octingen-pvt-ltd-31395a1a4/" target="_blank"><span
                                    class="icon-linkedin icomoon"></span></a>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ top header -->

        <!-- navigartion -->
        <div class="container nav-container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="index.html"> <img src="img/logo.svg" alt=""> </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.html">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.html">About</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Services
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="software-development.html">Software Development</a>
                                <a class="dropdown-item" href="data-analysis.html">Data Analysis</a>
                                <a class="dropdown-item" href="consulting.html">Consulting</a>
                                <a class="dropdown-item" href="data-science.html">Data Science</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="career.html">Careers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="blog.html">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">Contact</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!--/ navigation -->
    </header>
    <!--/ header -->

    <!-- main -->
    <!-- sub page header -->
    <div class="subpage-header">
        <!-- container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <article class="title-box wow bounceInUp">
                        <h1 class="h1"> Contact </h1>
                    </article>
                </div>
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page main -->
    <div class="subpage-main">
        <!-- container -->
        <div class="container main-container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">
                    <!-- brudcrumb-->
                    <nav>
                        <ol class="breadcrumb mb-0 wow bounceInUp">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> Contact </li>
                        </ol>
                    </nav>
                    <!--/ brudcrumb-->
                    <!-- sub pagein-->
                    <div clas="subpage-in">
                        <!-- row -->
                        <div class="row py-3 justify-content-center">
                            <!-- col -->
                            <div class="col-lg-8">
                                <h4 class="h3">Contact Us</h4>
                                <p>Thank you for your interest in Octingen. Please submit your information using the
                                    appropriate form depending on the type of your inquiry - Business, Vendor or Media.
                                    This information will help us route your request to the appropriate team. You should
                                    receive a response from our team within 2 working days.</p>

                                    <?php 
                                        if(isset($_POST['submit'])){
                                            
                                        $to = "info@octingen.com"; 
                                        $subject = "Mail From ".$_POST['name'];
                                        
                                        $message = "
                                        <html>
                                        <head>
                                        <title>HTML email</title>
                                        </head>
                                        <body>
                                        <p>".$_POST['name']." has sent mail!</p>
                                            <table>
                                                <tr>
                                                    <th align='left'>Name</th>
                                                    <td>".$_POST['name']."</td>
                                                </tr>
                                                <tr>
                                                    <th align='left'>Company Name</th>
                                                    <td>".$_POST['companyName']."</td>
                                                </tr>
                                                <tr>
                                                    <th align='left'>Email</th>
                                                    <td>".$_POST['email']."</td>
                                                </tr>
                                                <tr>
                                                    <th align='left'>Phone</th>
                                                    <td>".$_POST['phone']."</td>
                                                </tr>
                                                <tr>
                                                    <th align='left'>Message</th>
                                                    <td>".$_POST['msg']."</td>
                                                </tr>
                                            </table>
                                        </body>
                                        </html>
                                        ";

                                        // Always set content-type when sending HTML email
                                        $headers = "MIME-Version: 1.0" . "\r\n";
                                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                        // More headers
                                        $headers .= 'From:' .$_POST['name']. "\r\n";
                                        mail($to,$subject,$message,$headers);                            
                                        echo "<p style='color:green'> Mail Sent. Thank you " . $_POST['name'] .", we will contact you shortly. </p>";
                                        }
                                    ?>
                                    <!-- form -->
                                    <form class="form" method="post">
                                        <!-- row -->
                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Write Your Name" class="form-control" name="name" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->

                                            <!-- col -->
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Write Your Company Name" class="form-control" name="companyName" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->

                                            <!-- col -->
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Email </label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Write Your Email Address" class="form-control" name="email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->

                                            <!-- col -->
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Contact Number </label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Enter Valid Phone Number" class="form-control" name="phone" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->

                                            <!-- col -->
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Message </label>
                                                    <div class="input-group">
                                                        <textarea class="form-control" style="height:125px;"
                                                            placeholder="Write Your Custom Message, Not Mandatory" name="msg"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->

                                            <!-- col -->
                                            <div class="col-lg-12">                                           
                                                <input type="submit" value="submit" class="link" name="submit">
                                            </div>
                                            <!--/ col -->

                                        </div>
                                        <!--/ row -->
                                    </form>
                                    <!--/ form -->
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--- sub page in-->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page main -->
    <!--/ main -->
    <!--/ footer -->
    <footer>
        <!-- footer navigation-->
        <div class="footer-nav">
            <div class="container">
                <ul class="nav">
                    <li class="nav-item"><span>Quick Links:</span></li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.html">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="career.html">careers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
        <!--/ footer navigation -->

        <!-- bottom footer -->
        <div class="bottom-footer position-relative">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 footerbrand">
                        <a href="index.html"><img src="img/logo.svg" alt=""></a>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-5 footer-social">
                        <div>
                            <a href="https://www.facebook.com/Octingen-Pvt-Ltd-105527871073455" target="_blank">
                                <span class="icon-facebook icomoon"></span>
                                <span>Facebook</span>
                            </a>
                            <a href="https://twitter.com/octingen" target="_blank">
                                <span class="icon-twitter icomoon"></span>
                                <span>Twitter</span>
                            </a>
                            <a href="https://www.linkedin.com/in/octingen-pvt-ltd-31395a1a4/" target="_blank">
                                <span class="icon-linkedin icomoon"></span>
                                <span>Linkedin</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4 address align-self-center">
                        <p>© 2020 Octingen Technologies Pvt Ltd</p>
                        <p><span>mail:</span> info@octingen.com</p>
                    </div>
                    <!--/ col -->
                </div>
            </div>
            <!--/ container -->
            <a href="javascript:void(0)" id="movetop"><span class="icon-arrow-up icomoon"></span></a>
        </div>
        <!--/ bottom footer -->

    </footer>
    <!--/ footer -->

    <!-- footer scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script>
        new WOW().init();
    </script>
    <script src="js/custom.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5-shiv.js"></script>
    <![end if]-->
    <script src="js/baguetteBox.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', {
            animation: 'slideIn'
        });
    </script>
    <!--/ footer scripts -->
</body>

</html>